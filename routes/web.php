<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('landing');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::put('users/verify/{Id}','UserController@verify')->name('users.verify');
Route::resource('users', 'UserController');

Route::post('search','SearchController@search')->name('search');
Route::get('search','BookController@index')->name('search.index');

Route::get('books/createBook','BookController@createBook')->name('books.createBook');
Route::post('books/createBook','BookController@storeBook')->name('books.storeBook');
Route::resource('books', 'BookController');

Route::resource('authors', 'AuthorController');


Route::resource('bookList', 'BookListController');

Route::resource('bookIssueLog','BookIssueLogController');
//Route::resource('books', 'BookController');


/*
 * uzytkownicy:
 *
 * dodawanie ksiazek
    * dodawanie egzemplazy
 * wypozyczanie ksiazek
    * zatwierdzanie ksiazek

*/
