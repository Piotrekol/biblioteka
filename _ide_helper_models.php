<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Author
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $country
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Book[] $book
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Author whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Author whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Author whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Author whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Author whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Author whereUpdatedAt($value)
 */
	class Author extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Book
 *
 * @property int $id
 * @property int $authorId
 * @property string $title
 * @property string $desc
 * @property string $releaseDate
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Author $author
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\bookIssue[] $issue
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereReleaseDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereUpdatedAt($value)
 */
	class Book extends \Eloquent {}
}

namespace App{
/**
 * App\bookIssue
 *
 * @property int $id
 * @property int $bookId
 * @property int $avaliable
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Book $book
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssue whereAvaliable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssue whereBookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssue whereUpdatedAt($value)
 */
	class bookIssue extends \Eloquent {}
}

namespace App{
/**
 * App\bookIssueLog
 *
 * @property int $id
 * @property int $issueId
 * @property int $userId
 * @property int|null $librarianId
 * @property string $issueDeadline
 * @property int $approved
 * @property \Carbon\Carbon|null $issuedAt
 * @property \Carbon\Carbon|null $returnDate
 * @property int $returned
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\bookIssue $issue
 * @property-read \App\User $librarian
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssueLog whereApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssueLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssueLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssueLog whereIssueDeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssueLog whereIssueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssueLog whereIssuedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssueLog whereLibrarianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssueLog whereReturnDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssueLog whereReturned($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssueLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\bookIssueLog whereUserId($value)
 */
	class bookIssueLog extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User role($roles)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

