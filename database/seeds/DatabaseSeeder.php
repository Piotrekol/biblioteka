<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Book;
use App\bookIssue;
use App\Models\Author;
use \Carbon\Carbon;
use App\bookIssueLog;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(User::count()!==0)
            return;
        $this->call(RolesAndPermissionsSeeder::class);

        //add authors
        $authors = [
            ['name'=>'n1','surname'=>'s1','country'=>'c1'],
            ['name'=>'n2','surname'=>'s2','country'=>'c2'],
            ['name'=>'n3','surname'=>'s3','country'=>'c3'],
            ['name'=>'n4','surname'=>'s4','country'=>'c4'],
            ['name'=>'n5','surname'=>'s5','country'=>'c5'],
        ];
        foreach($authors as $author){
            Author::create($author);
        }
        //add users
        $users = [
            ['name' => 'admin','address'=>'adres_','phone'=>'123456789', 'surname'=>'admin', /*'username' => 'admin', 'verified'=>1,*/ 'email' => 'admin@pioo.space', 'password' => bcrypt('password')],
            ['name' => 'librarian','address'=>'adres_','phone'=>'123456789', 'surname'=>'librarian', /*'username' => 'librarian', 'verified'=>1,*/ 'email' => 'librarian@pioo.space', 'password' => bcrypt('password')],
            ['name' => 'user','address'=>'adres_', 'surname'=>'user','phone'=>'123456789',/* 'username' => 'user', 'verified'=>1, */'email' => 'user@pioo.space', 'password' => bcrypt('password')],
            ['name' => 'verifiedUser','address'=>'adres_', 'surname'=>'verifiedUser','phone'=>'123456789',/* 'username' => 'verifiedUser', 'verified'=>0,*/ 'email' => 'verifiedUser@pioo.space', 'password' => bcrypt('password')],
        ];
        $i=0;
        foreach($users as $user){
            //$user[] = ['address'=>'adres_'.$i,'phone'=>'123456789'.$i];
            User::create($user);
            //$i++;
        }

        //add books
        $books = [
            ['title'=>'t1','desc'=>'d1','releaseDate'=>Carbon::now(),'authorId'=>1,'genre'=>'genre1'],
            ['title'=>'t2','desc'=>'d2','releaseDate'=>Carbon::now(),'authorId'=>1,'genre'=>'genre2'],
            ['title'=>'t3','desc'=>'d3','releaseDate'=>Carbon::now(),'authorId'=>1,'genre'=>'genre2'],
            ['title'=>'t4','desc'=>'d4','releaseDate'=>Carbon::now(),'authorId'=>3,'genre'=>'genre2'],
            ['title'=>'t5','desc'=>'d5','releaseDate'=>Carbon::now(),'authorId'=>1,'genre'=>'genre2'],
            ['title'=>'t6','desc'=>'d6','releaseDate'=>Carbon::now(),'authorId'=>1,'genre'=>'genre3'],
            ['title'=>'t7','desc'=>'d7','releaseDate'=>Carbon::now(),'authorId'=>2,'genre'=>'genre3'],
            ['title'=>'t8','desc'=>'d8','releaseDate'=>Carbon::now(),'authorId'=>2,'genre'=>'genre4'],
            ['title'=>'t9','desc'=>'d9','releaseDate'=>Carbon::now(),'authorId'=>1,'genre'=>'genre1'],
        ];
        foreach($books as $book){
            Book::create($book);
        }
        //add book issues
        $bookIssues=[
            ['bookId'=>1],['bookId'=>1],
            ['bookId'=>2],['bookId'=>2],['bookId'=>2],
            ['bookId'=>4],['bookId'=>4],['bookId'=>4],['bookId'=>4],
            ['bookId'=>5],['bookId'=>5],['bookId'=>5],['bookId'=>5],

        ];
        foreach($bookIssues as $bookIssue){
            bookIssue::create($bookIssue);
        }
        //'issueId','userId','librarianId','issuedAt','returnDate'
        $bookIssueLogs=[
            ['issueId'=>1,'userId'=>3,'approved'=>0,'librarianId'=>2,'issueDeadline'=>Carbon::now()->addDays(2),'returnDate'=>Carbon::now()->addDays(2)],
            ['issueId'=>2,'userId'=>3,'approved'=>0,'librarianId'=>2,'issueDeadline'=>Carbon::now()->addDays(2),'returnDate'=>Carbon::now()->addDays(2)],
            ['issueId'=>3,'userId'=>3,'approved'=>1,'librarianId'=>2,'issueDeadline'=>Carbon::now()->addDays(2),'issuedAt'=>Carbon::now()->subDays(4),'returnDate'=>Carbon::now()->addDays(5)],
            ['issueId'=>4,'userId'=>3,'approved'=>1,'librarianId'=>2,'issueDeadline'=>Carbon::now()->addDays(2),'issuedAt'=>Carbon::now()->subDays(15),'returnDate'=>Carbon::now()->addDays(2)],
            ['issueId'=>5,'userId'=>3,'approved'=>1,'librarianId'=>2,'issueDeadline'=>Carbon::now()->addDays(2),'issuedAt'=>Carbon::now(),'returnDate'=>Carbon::now()->addDays(20)],
            ['issueId'=>6,'userId'=>3,'approved'=>1,'librarianId'=>2,'issueDeadline'=>Carbon::now()->addDays(2),'issuedAt'=>Carbon::now()->subDays(1),'returnDate'=>Carbon::now()->addDays(25)],
            ['issueId'=>7,'userId'=>3,'approved'=>1,'librarianId'=>2,'issueDeadline'=>Carbon::now()->addDays(2),'issuedAt'=>Carbon::now()->subDays(4),'returnDate'=>Carbon::now()->addDays(30)],
            ['issueId'=>8,'userId'=>3,'approved'=>0,'librarianId'=>2,'issueDeadline'=>Carbon::now()->addDays(2),'returnDate'=>Carbon::now()->addDays(2)],
            ['issueId'=>9,'userId'=>3,'approved'=>0,'librarianId'=>2,'issueDeadline'=>Carbon::now()->addDays(2),'returnDate'=>Carbon::now()->addDays(2)],
        ];
        foreach ($bookIssueLogs as $bookIssueLog){
            bookIssueLog::create($bookIssueLog);
        }

        //set premissions
        $user = User::where('name','admin')->first();
        $user->assignRole('admin');

        $user = User::where('name','librarian')->first();
        $user->assignRole('librarian');

        $user = User::where('name','user')->first();
        $user->assignRole('user');

        $user = User::where('name','verifiedUser')->first();
        $user->assignRole('verifiedUser');




    }

}
