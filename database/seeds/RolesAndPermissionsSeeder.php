<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');
        //Uprawnienia:
        Permission::create(['name' => 'changePremissions']); //zmiana uprawnien uzytkownikow

        Permission::create(['name' => 'issueBooks']);//dodawanie egzemplazy ksiazek
        Permission::create(['name' => 'seeIssues']);//przegladanie wydań/egzemplazy ksiazek
        Permission::create(['name' => 'seeIssueLog']);//przegladanie wypozyczeń

        Permission::create(['name' => 'verifyIssueLog']);//przegladanie wydań/egzemplazy ksiazek
        Permission::create(['name' => 'verifyIssues']);//Zatwierdzanie wypozyczen i zwrotów

        Permission::create(['name' => 'addRemoveBooks']);//dodawanie/usuwanie ksiazek

        Permission::create(['name' => 'createUsers']);//Twozenie uzytkownikow
        Permission::create(['name' => 'editUsers']);//Edycja uzytkownikow
        Permission::create(['name' => 'verifyUsers']);//zatwierdzanie rejestracji uzytkownikow

        //Role:
        $role = Role::create(['name' => 'admin']);//.. wszystko
        $role->givePermissionTo('changePremissions');
        $role->givePermissionTo('issueBooks');
        $role->givePermissionTo('seeIssues');
        $role->givePermissionTo('addRemoveBooks');
        $role->givePermissionTo('createUsers');
        $role->givePermissionTo('editUsers');
        $role->givePermissionTo('verifyUsers');
        $role->givePermissionTo('verifyIssueLog');
        $role->givePermissionTo('verifyIssues');

        $role = Role::create(['name' => 'librarian']);//Bibliotekaz
        $role->givePermissionTo('issueBooks');
        $role->givePermissionTo('seeIssues');
        $role->givePermissionTo('addRemoveBooks');
        $role->givePermissionTo('verifyUsers');
        $role->givePermissionTo('seeIssueLog');
        $role->givePermissionTo('verifyIssueLog');

        $role = Role::create(['name' => 'user']);

        $role = Role::create(['name' => 'rejectedUser']);

        $role = Role::create(['name' => 'verifiedUser']);//zatwierdzony - ma możliwość wypożyczenia ksiązki
        $role->givePermissionTo('issueBooks');
        $role->givePermissionTo('seeIssueLog');


    }
}
