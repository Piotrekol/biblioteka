<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookIssueLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_issue_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('issueId');
            $table->integer('userId');
            $table->integer('librarianId')->nullable();
            $table->dateTime('issueDeadline');
            $table->boolean('approved')->default(0);
            $table->dateTime('issuedAt')->nullable();
            $table->dateTime('returnDate')->nullable();
            $table->boolean('returned')->default(0);

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_issue_logs');
    }
}
