<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //zawiera poszczegolne egzemplaze danych ksiazek
        Schema::create('book_issues', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bookId');
            $table->boolean("avaliable")->default(1);
            //$table->int('AdderId');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_issues');
    }
}
