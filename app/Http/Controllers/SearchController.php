<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request){
        $this->validate($request, [
            'searchString'=>'present'
        ]);
        $resultBooks = Book::search(request('searchString'))->paginate(15);
        //if($resultBooks->count()>0)
        //    return $resultBooks;
        return view('books.index')->with('books', $resultBooks)
            ->with('searchAd',"<img src=\"https://www.algolia.com/static_assets/images/press/downloads/search-by-algolia.png\"></img>");


        //$resultAuthors = Book::search(request('searchString'))->get();
        //return $resultAuthors;
    }
}
