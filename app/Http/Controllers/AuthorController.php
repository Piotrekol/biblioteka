<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Author;

class AuthorController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'anyRole:admin,librarian']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('author.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'=>'required|max:120',
            'surname'=>'required|max:120',
            'country'=>'required|min:2|max:2'
        ]);
        Author::create($request->only(['name','surname','country']));

        return redirect()->route('books.createBook')
            ->with('flash_message', 'Autor dodany');
    }
}
