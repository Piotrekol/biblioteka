<?php

namespace App\Http\Controllers;

use App\bookIssue;
use App\bookIssueLog;
use App\Models\Author;
use App\Models\Book;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ViewErrorBag;
use Illuminate\Contracts\Validation\Rule;

class apiController extends Controller
{
    private $resultsPerPage = 20;
    function getAuthor(int $authorId){
        $q = Author::Query();
        return $q->where('id',$authorId)->get()->toJson();
    }
    function getAuthors(int $page){
        $q = Author::Query();

        $paginator = $q->paginate($this->resultsPerPage,['*'],'page',$page);

        return $paginator;
    }
    function getBooks(Request $request){
        $books = Book::with('author')->withCount('issue')->get();
        return $books->toJson();
    }

    public function reserveBook(Request $request){
        $this->validate($request, [
            'bookId'=>[
                        'required',
                        'exists:books,id',
                        'exists:book_issues,bookId,avaliable,1',
                        new LoggedInRule(),
                        new NotVerifiedRule(),
                        new AlreadyHasThisBookRule(request('bookId')),
                        new NotReturnedXBooksRule(),
                        new ReservedXBooksRule(),
                        new AvaliableBookRule(request('bookId')),
                     ],
        ]);
        $response =[];
        $bookId =(int)request('bookId');
        $bookIssue = Book::find($bookId)->freeIssue();

        bookIssueLog::CreateNew(Auth::user()->id,$bookIssue->id,Carbon::now()->addDays(2));
        return response()->json([], 204);
    }
}

class AvaliableBookRule implements Rule
{
    private $bookId=-1;

    public function __construct($bookId)
    {
        $this->bookId = $bookId;
    }

    public function passes($attribute, $value)
    {
        if(!Auth::user())
            return false;
        $book = Book::find($this->bookId)->first();

        return $book->avaliableCount()>0;
    }

    public function message()
    {
        return 'Brak dostępnych egzemplazy podanej ksiazki!';
    }
}
class ReservedXBooksRule implements Rule
{
    private $issueLimit=5;
    public function passes($attribute, $value)
    {
        if(!Auth::user())
            return false;
        $issuedBooksCount = bookIssueLog::where('userId',Auth::user()->id)->where('approved',0)->count();

        return $issuedBooksCount < $this->issueLimit;
    }

    public function message()
    {
        return 'Nie można zaarezerwować wiecej niz '.$this->issueLimit.' książek!';
    }
}
class NotReturnedXBooksRule implements Rule
{
    private $returnLimit=5;

    public function passes($attribute, $value)
    {
        if(!Auth::user())
            return false;
        $notReturnedBooksCount = bookIssueLog::where('userId',Auth::user()->id)
            ->where(function ($query) {
                $query->where('approved',1)->where('returned',0);
            })->count();

        return $notReturnedBooksCount<5;
    }

    public function message()
    {
        return 'Nie można wypożyczyć wiecej niz '.$this->returnLimit.' książek!';
    }
}
class AlreadyHasThisBookRule implements Rule
{
    private $bookId=-1;

    public function __construct($bookId)
    {
        $this->bookId = $bookId;
    }
    public function passes($attribute, $value)
    {
        if(!Auth::user() or $this->bookId==null)
            return false;
        $userBooks = bookIssueLog::GetNotReturnedIssues()->with('issue')->get();
        foreach ($userBooks as $userbook){
            if($userbook->issue->bookId == $this->bookId)
                return false;
        }
        return true;
    }

    public function message()
    {
        return 'Ta ksiazka jest juz zarezerwowana badz w twoim posiadaniu!';
    }
}

class LoggedInRule implements Rule
{

    public function passes($attribute, $value)
    {
        return Auth::user()!=null;
    }

    public function message()
    {
        return "Prosze się zalogować!";
    }
}
class NotVerifiedRule implements Rule
{

    public function passes($attribute, $value)
    {
        if(!Auth::user())
            return false;
        return !Auth::user()->hasRole('user');
    }

    public function message()
    {
        return "To konto nie zostalo jeszcze zatwierdzone!";
    }
}