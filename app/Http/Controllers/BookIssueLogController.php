<?php

namespace App\Http\Controllers;

use App\bookIssue;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\bookIssueLog;
use Illuminate\Support\Facades\Auth;

class BookIssueLogController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'anyRole:admin,librarian,verifiedUser']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $allUsers = $user->hasAnyRole(['admin','librarian']);
        if($allUsers){
            $bookIssueLogs = bookIssueLog::with('user');
        }else{
            $bookIssueLogs = bookIssueLog::where('userId',$user->id);
        }
        $result = $bookIssueLogs->with('librarian')->with('issue.book')->withTrashed()->paginate(15);
        return view('bookIssueLog.index')
            ->with('bookIssueLogs', $result)
            ->with('allUsers',$allUsers);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'issueId'=>'required|exists:book_issues,id,avaliable,1',
        ]);
        $user = Auth::user();
        $error=false;
        $errorMessage=null;
        $issuedBooksCount = bookIssueLog::where('userId',$user->id)->where('approved',0)->count();
        $notReturnedBooksCount = bookIssueLog::where('userId',$user->id)
            ->where(function ($query) {
                $query->where('approved',1)->where('returned',0);
            })->count();
        if($issuedBooksCount>=5){
            $error=true;
            $errorMessage="Nie można zaarezerwować wiecej niz 5 książek!";
        }elseif($notReturnedBooksCount>=5){
            $error=true;
            $errorMessage="Nie można wypożyczyć wiecej niz 5 książek!";
        }

        $book = bookIssue::find((int)request('issueId'));

        $userBooks = bookIssueLog::GetNotReturnedIssues()->with('issue')->get();
        foreach ($userBooks as $userbook){
            if($userbook->issue->bookId == $book->bookId)
            {
                $error=true;
                $errorMessage="Ta ksiazka jest juz zarezerwowana badz w twoim posiadaniu!";
            }
        }
        if($error){
            return redirect()->back()->withErrors(['error'=>$errorMessage]);
        }
        $params = $request->only('issueId');

        bookIssueLog::CreateNew((int)$user->id,(int)$params['issueId'],Carbon::now()->addDays(2));
        return redirect()->back()->with('flash_message',
                'Ksiażka zarezerwowana na okres 2 dni');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'action'=>'in:return,approve'
        ]);
        if(!Auth::user()->hasPermissionTo('verifyIssueLog')){
            abort(401,"Brak uprawnień");
        }
        $bookIssueLog = bookIssueLog::where('id',(int)$id)->first();
        $action = $request['action'];
        if($action ==='return')
            $bookIssueLog->return();
        else{
            $bookIssueLog->approve();
        }
        $bookIssueLog->save();

        $message = $request['action']=='return'? 'Potwierdzono zwrot':' Potwierdzono wydanie';
        return redirect()->back()->with('flash_message',$message);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::user()->hasPermissionTo('verifyIssueLog')){
            $bookIssueLog = bookIssueLog::findOrFail($id);
            if($bookIssueLog->userId === Auth::user()->id){
                if($bookIssueLog->approved )
                    abort(401,"Brak uprawnień");
            }
        }
        else
            $bookIssueLog = bookIssueLog::findOrFail($id);
        $bookIssueLog->delete();

        return redirect()->back()
            ->with('flash_message','Rezerwacja usunieta.');
    }
}
