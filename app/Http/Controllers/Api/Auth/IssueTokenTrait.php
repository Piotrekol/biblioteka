<?php
namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;

trait IssueTokenTrait {
    private $client;

    public function __construct()
    {
        $this->client = Client::find(1);
    }
    public function issueToken(Request $request, $grantType, $scope = ""){
        $params = [
            'grant_type' => $grantType,
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'scope' => $scope
        ];
        if($grantType !== 'social'){
            $params['username'] = $request->username ?: $request->email;
        }
        $request->request->add($params);

        $proxy = Request::create('oauth/token', 'POST');

        return Route::dispatch($proxy);
    }
}