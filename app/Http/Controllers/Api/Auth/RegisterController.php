<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;

class RegisterController extends Controller
{
    use IssueTokenTrait;

    private $client;

    public function __construct()
    {
        $this->client = Client::find(1);
    }

    function register(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'surname'=>'required|max:120',
            'address'=>'required|max:200',
            'phone' => 'required|regex:/[0-9]{9,11}/',
            //'surname' => 'required|string|max:50',
            //'username' =>'required|string|max:15'
        ]);
        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'surname' =>request('surname'),
            'address'=>request('address'),
            'phone'=> request('phone')
        ]);
        //Default not-verified role
        $user->assignRole("user");

        return $this->issueToken($request,'password');

    }
}
