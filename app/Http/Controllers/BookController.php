<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\bookIssue;
use App\Models\Book;
use App\Models\Author;
class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::with('issue')->with('author')->paginate(15);

        return view('books.index')->with('books', $books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $books = Book::all();
        return view('books.add', ['books'=>$books]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'bookId'=>'required|exists:books,id',
            'count'=>'required|integer|min:1|max:100',
        ]);

        $newBooksCount = (int)$request['count'];
        for ($i=0;$i<$newBooksCount;$i++){
            bookIssue::create($request->only('bookId'));
        }
        $message = $newBooksCount>1 ? 'Ksiazki dodane pomyślnie':'Ksiazka dodana pomyślnie';
        return redirect()->route('books.index')
            ->with('flash_message', $message);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        $book->delete();

        return redirect()->route('books.index')
            ->with('flash_message',
                'Ksiazka usunieta pomyślnie.');
    }


    public function createBook(){
        $authors = Author::all();
        return view('books.create',['authors'=>$authors]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeBook(Request $request)
    {
        $this->validate($request, [
            'title'=>'required|max:120',
            'authorId'=>'required|exists:authors,id',
            'desc'=>'required|max:1024',
            'releaseDate'=>'required|date',
            'genre'=>'required|string|max:50'
        ]);
        Book::create($request->only(['title','authorId','desc','releaseDate','genre']));

        return redirect()->route('books.index')
            ->with('flash_message', 'Ksiazka dodana pomyślnie');
    }
}
