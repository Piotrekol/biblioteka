<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\bookIssue;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use Searchable,SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $guarded = ['id'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','created_at','updated_at','authorId'
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'authorId', 'title', 'desc', 'releaseDate','genre'
    ];
    protected $appends = ['avaliable_count'];

    protected static function boot() {
        parent::boot();

        static::deleting(function($book) { // before delete() method call this
            $book->issue()->delete();
        });
    }



    public function author() {
        return $this->hasOne('App\Models\Author','id','authorId');
    }
    public function issue() {
        return $this->hasMany('App\bookIssue','bookId','id');
    }
    public function avaliableCount(){
        $c = bookIssue::where('bookId',$this->id)->where('avaliable',1)->count();
        return $c;
    }
    private $freeIssue=null;
    public function freeIssue(){
        if($this->freeIssue)
            return $this->freeIssue;
        $this->freeIssue = bookIssue::where('bookId',$this->id)->where('avaliable',1)->first();
        return $this->freeIssue;
    }
    public function getAvaliableCountAttribute($value)
    {
        return $this->avaliableCount();
    }
}
