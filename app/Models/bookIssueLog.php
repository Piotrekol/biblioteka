<?php

namespace App;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class bookIssueLog extends Model
{
    use SoftDeletes;
    protected static function boot() {
        parent::boot();

        static::deleting(function($bookIssueLog) { // before delete() method call this
            //delete all revelant book logs.
            $bookIssueLog->issue->return();
        });
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'issueId','issueDeadline','approved','returned','userId','librarianId','issuedAt','returnDate'
    ];
    protected $dates = ['issuedAt','returnDate','deleted_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id'
    ];
    public static function CreateNew($userId,$issueId,$deadline){
        bookIssue::find((int)$issueId)->take();
        return bookIssueLog::create(
            ['issueId'=>$issueId,'userId'=>$userId,'issueDeadline'=>$deadline]
        );
    }

    /**
     * @return $this
     */
    public static function GetNotReturnedIssues(){
        return bookIssueLog::Query()->where('userId',Auth::user()->id)->where(function ($query) {
            $query->where('approved',0)
                ->orWhere(function($query){
                    $query->where('approved',1)->where('returned',0);
                });
        });
    }
    public function return(){
        $this->returned = true;
        $this->returnDate = Carbon::now();
        $this->issue->return();
    }
    public function approve(){
        $this->approved=true;
        $this->librarianId = Auth::user()->id;
        $this->issuedAt=Carbon::now();
        $this->returnDate = Carbon::now()->addDays(30);
        $this->issue->take();


    }
    public function user() {
        return $this->hasOne('App\User','id','userId')->withTrashed();
    }
    public function issue() {
        return $this->hasOne('App\bookIssue','id','issueId')->withTrashed();
    }
    public function librarian() {
        return $this->hasOne('App\User','id','librarianId')->withTrashed();
    }
}
