<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class bookIssue extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bookId','avaliable'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id'
    ];
    protected static function boot() {
        parent::boot();

        static::deleting(function($bookIssue) { // before delete() method call this
            //delete all revelant book logs.
            $bookIssue->log()->delete();
        });
    }
    public function return(){
        $this->avaliable=1;
        $this->save();
    }
    public function take(){
        $this->avaliable=0;
        $this->save();
    }

    public function book() {
        return $this->hasOne('App\Models\Book','id','bookId')->withTrashed();
    }

    public function log() {
        return $this->hasMany('App\bookIssueLog','issueId','id')->withTrashed();
    }
}
