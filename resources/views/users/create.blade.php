@extends('layouts.app')

@section('title', '| Dodaj uzytkownika')

@section('content')

    <div class='container'>



        <div class="divide">
        {{ Form::open(array('url' => 'users')) }}

        <div class="form-group">
            {{ Form::label('name', 'Nazwa') }}
            {{ Form::text('name', '', array('class' => 'form-control')) }}
        </div>

        <div class="form-group">
            {{ Form::label('email', 'Email') }}
            {{ Form::email('email', '', array('class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('password', 'hasło') }}
            {{ Form::password('password', array('class' => 'form-control')) }}

        </div>

        <div class="form-group">
            {{ Form::label('password', 'Potwierdź hasło') }}
            {{ Form::password('password_confirmation', array('class' => 'form-control')) }}

        </div><br/>
        <div class='form-group'>
            @foreach ($roles as $role)
                {{ Form::checkbox('roles[]',  $role->id ) }}
                {{ Form::label($role->name, ucfirst($role->name)) }}

            @endforeach
        </div>
            <br/>

        <div>
        {{ Form::submit('Dodaj', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}
        </div>
    </div>
    </div>
@endsection