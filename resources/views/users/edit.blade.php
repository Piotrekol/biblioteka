@extends('layouts.app')

@section('title', '| Edytuj uzytkownika')

@section('content')

    <div class='container'>




        {{--automatyczne wypelnienie danych poprzez polaczenie z modelem uzytkownika eloquent --}}
        {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT')) }}

        <div class="form-group">
            {{ Form::label('name', 'Imie') }}
            {{ Form::text('name', null, array('class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('surname', 'Nazwisko') }}
            {{ Form::text('surname', null, array('class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('email', 'Email') }}
            {{ Form::email('email', null, array('class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('addres', 'Adres zamieszkania') }}
            {{ Form::text('address', null, array('class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('phone', 'Numer telefonu') }}
            {{ Form::text('phone', null, array('class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('password', 'Hasło') }}
            {{ Form::password('password', array('class' => 'form-control')) }}

        </div>

        <div class="form-group">
            {{ Form::label('password', 'Potwierdź hasło') }}
            {{ Form::password('password_confirmation', array('class' => 'form-control')) }}

        </div>
        <br/>

        <div class='form-group'>
            @foreach ($roles as $role)
                {{ Form::checkbox('roles[]',  $role->id, $user->roles ) }}
                {{ Form::label($role->name, ucfirst($role->name)) }}

            @endforeach
        </div>
        <br/>


        {{ Form::submit('Edytuj', ['class' => 'btn btn-primary']) }}

        {{ Form::close() }}

    </div>

@endsection