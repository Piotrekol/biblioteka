@extends('layouts.app')

@section('title', '| Uzytkownicy')

@section('content')


    <div class="container">

            <table id="t_test" class="table table-bordered table-striped">

                <thead>
                <tr>
                    <th>Nazwa</th>
                    <th>Email</th>
                    <th>Data dodania</th>
                    <th>Role uzytkownika</th>
                    <th>Operacje</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($users as $user)
                    <tr>

                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at->format('F d, Y h:ia') }}</td>
                        <td>{{  $user->roles()->pluck('name')->implode(' ') }}</td>{{-- Retrieve array of roles associated to a user and convert to string --}}

                        <td class="wazne">

                            @can('editUsers')
                                {!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user->id] ]) !!}
                                    <div class="btn-group">
                                        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-info">Edytuj</a>
                                        @if( Auth::user()->id != $user->id )
                                            {!! Form::submit('Usuń', ['class' => 'btn btn-danger']) !!}
                                        @endif
                                    </div>
                                {!! Form::close() !!}
                            @endcan
                            @can('verifyUsers')
                                @if($user->hasRole('user'))
                                    {!! Form::open(['method' => 'PUT', 'route' => ['users.verify', $user->id] ]) !!}
                                        {!! Form::submit('Zatwierdz', ['class' => 'btn btn-success']) !!}
                                    {!! Form::close() !!}
                                @endif
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>

        @can('createUsers')
            <a href="{{ route('users.create') }}" class="btn btn-success">Dodaj użytkownika</a>
        @endcan
        {{$users->links()}}

    </div>

@endsection