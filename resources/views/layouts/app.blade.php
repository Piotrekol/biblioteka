<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} @yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>





    <div id="app">
        <div class="main_panel">
        <div class="l_div"><img src="/img/logo.png" class="logo"></img></div>
            <div class="logging">
                        @guest


                           <div class="mail"> <a href="{{ route('login') }}">LOGOWANIE</a></div>
                            <a href="{{ route('register') }}">REJESTRACJA</a>
                        @else

<div class="mail">
                                    {{ Auth::user()->email }}
</div>


                                            <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();" class="logout">
                                                WYLOGUJ SIĘ
                                            </a>



                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>



                        @endguest
            </div>
        </div>





        <div class="buttons">


            @if(Gate::check('verifyUsers'))
                <div class="admin_buttons">

                <a href="{{route('users.index')}}">  <div class="b_1"> <span>UŻYTKOWNICY</span> </div></a>



                    <a href="{{route('books.index')}}"> <div class="b_2"> <span>  DODAJ KSIĄŻKĘ  </span> </div></a>



                    <a href="{{route('bookIssueLog.index')}}">  <div class="b_3"> <span>  WYPOŻYCZENIA   </span> </div></a>



                    <a href="{{route('bookList.index')}}">  <div class="b_4"><span>SPIS KSIĄŻEK</span> </div></a>

                </div>


            @elseif(Gate::check('seeIssueLog'))
                <div class="verified_user_buttons">
                    <a href="{{route('landing')}}">  <div class="b_4"><span>STRONA GŁÓWNA</span> </div></a>
                    <a href="{{route('bookIssueLog.index')}}">  <div class="b_3"> <span>  WYPOŻYCZENIA   </span> </div></a>

                    <a href="{{route('bookList.index')}}">  <div class="b_4"><span>SPIS KSIĄŻEK</span> </div></a>


                </div>




                @else
                <div class="all_users_buttons">
                    <a href="{{route('landing')}}">  <div class="b_4"><span>STRONA GŁÓWNA</span> </div></a>
                    <a href="{{route('bookList.index')}}">  <div class="b_4"><span>SPIS KSIĄŻEK</span> </div></a>


                </div>
                @endif





        </div>







        @if(Session::has('flash_message'))
                <div class="alert alert-success"><em> {!! session('flash_message') !!}</em>
                </div>
        @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                    </div>
                @endif

        @yield('content')

    </div>

    <!-- Scripts -->

</body>
<footer>
    <div class="projekt">PROJEKT "BIBLIOTEKA"</div>
    <div class="pwsz">@2017 PWSZ LEGNICA</div>
</footer>
</html>
