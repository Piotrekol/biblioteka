@extends('layouts.app')

@section('title', '| Uzytkownicy')

@section('content')

    <div class="container">

        {{ Form::open(array('url' => 'search')) }}
        @include('search.search')

        {{ Form::close() }}


        <br />
            <table id="t_test" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Tytuł</th>
                    <th>opis</th>
                    <th>data wydania</th>
                    <th>autor</th>
                    <th>operacje</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($books as $book)
                    <tr>
                        <td>{{ $book->title }}</td>
                        <td>{{ $book->desc }}</td>
                        <td>{{ $book->releaseDate }}</td>
                        <td>{{ $book->author->name }} {{$book->author->surname}}</td>
                        <td>
                            @can('issueBooks')
                                @if($book->avaliableCount()>0)
                                    {{ Form::open(array('url' => 'bookIssueLog')) }}
                                    {{ Form::hidden('userId', Auth::user()->id) }}
                                    {{ Form::hidden('issueId', $book->freeIssue()->id) }}
                                    {!! Form::submit('Zarezerwuj', ['class' => 'btn btn-success']) !!}

                                    {{ Form::close() }}
                                @else
                                    <div>Brak egzemplazy na stanie</div>
                                @endif
                            @else
                                <div>Zaloguj bądź potwierdz swoje konto aby zarezerwowac!</div>
                            @endcan
                            {{--{!! Form::open(['method' => 'DELETE', 'route' => ['books.destroy', $book->id] ]) !!}
                            {!! Form::submit('Usuń', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}--}}

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        {{$books->links()}}

    </div>
@endsection