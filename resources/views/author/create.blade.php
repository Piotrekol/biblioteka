@extends('layouts.app')

@section('title', '| Dodaj autora')

@section('content')

    <div class='container'>



        {{ Form::open(array('url' => 'authors')) }}

        <div class="form-group">
            {{ Form::label('name', 'Imie') }}
            {{ Form::text('name', '', array('class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('name', 'Nazwisko') }}
            {{ Form::text('surname', '', array('class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('name', 'Kod kraju (2 znaki)') }}
            {{ Form::text('country', '', array('class' => 'form-control')) }}
        </div><br/>
        {{ Form::submit('Dodaj', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}

    </div>

@endsection