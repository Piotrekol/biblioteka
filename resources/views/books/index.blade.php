@extends('layouts.app')

@section('title', '| Uzytkownicy')

@section('content')

    <div class="container">
        {{ Form::open(array('url' => 'search')) }}
        @include('search.search')

        {{ Form::close() }}
        <hr>

            <table id="t_test"  class="table table-bordered table-striped">

                <thead>
                <tr>
                    <th>Id</th>
                    <th>Ksiazka</th>
                    <th>Ilosc</th>
                    <th>dostępne</th>
                    <th>gatunek</th>
                    <th>Data dodania</th>
                    <th>Operacje</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($books as $book)
                    <tr>

                        <td>{{ $book->id }}</td>
                        <td>{{ $book->title }}</td>
                        <td>{{ count($book->issue) }}</td>
                        <td>{{ $book->avaliableCount() }}</td>
                        <td>{{ $book->genre }}</td>
                        <td>{{ $book->created_at }}</td>

                        {{--<td>{{ $bookIssue->avaliable ? "Tak":"Nie" }}</td>
                        <td>{{ $bookIssue->created_at }}</td>
--}}
                        <td>
                            {!! Form::open(['method' => 'DELETE', 'route' => ['books.destroy', $book->id] ]) !!}
                                {!! Form::submit('Usuń', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}

                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>


        <a href="{{ route('books.create') }}" class="btn btn-success">Dodaj egzemplaż</a>
        <a href="{{ route('books.createBook') }}" class="btn btn-success">Dodaj nową książke</a>
        {!! $searchAd or '' !!}
        {{$books->links()}}

    </div>

@endsection