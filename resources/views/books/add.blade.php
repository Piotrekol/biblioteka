@extends('layouts.app')

@section('title', '| Dodaj uzytkownika')

@section('content')



     <div class="container">

        {{ Form::open(array('url' => 'books')) }}

        <div class="form-group">
            {{ Form::label('book', 'Książka') }}
            <select name="bookId">
                <option value="">Wybierz ksiazke...</option>
                @foreach($books as $book)
                    <option value="{{$book->id}}">{{$book->title}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            {{ Form::label('count', 'Ilosc nowych egzemplazy') }}
            {{ Form::number('count', '1') }}
        </div><br/>
        {{ Form::submit('Dodaj', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}
     </div>
    </div>

@endsection