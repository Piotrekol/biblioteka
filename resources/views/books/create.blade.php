@extends('layouts.app')

@section('title', '| Dodaj ksiazke')

@section('content')

    <div class='container'>




        {{ Form::open(array('url' => route('books.storeBook'))) }}

        <div class="form-group">
            {{ Form::label('title', 'tytuł') }}
            {{ Form::text('title', '', array('class' => 'form-control')) }}
        </div>

        <div class="form-group">
            {{ Form::label('book', 'Autor') }}
            <select name="authorId">
                <option value="">Wybierz autora...</option>
                @foreach($authors as $author)
                    <option value="{{$author->id}}">{{$author->name}}</option>
                @endforeach
            </select>
            <a href="{{ route('authors.create') }}" class="btn btn-success">Dodaj nowego autora</a>

        </div>
        <div class="form-group">
            {{ Form::label('desc', 'opis') }}
            {{ Form::textarea('desc', '', array('class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('genre', 'gatunek') }}
            {{ Form::textarea('genre', '', array('class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('releaseDate', 'Data wydania') }}
            {{ Form::date('releaseDate', '', array('class' => 'form-control')) }}
        </div><br/>
        {{ Form::submit('Dodaj', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}

    </div>

@endsection