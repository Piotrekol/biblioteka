
@extends('layouts.app')

@section('title', '| Wypożyczenia ksiazek')

@section('content')

    <div class="container">


            <table id="t_test" class="table table-bordered table-striped">

                <thead>
                <tr>
                    @if($allUsers)
                        <th>Uzytkownik</th>
                    @endif
                    <th>Ksiażka</th>
                    <th>Wydana</th>
                    <th>Termin wydania</th>
                    <th>Wydana przez</th>
                    <th>Data wydania</th>
                    <th>Data zwrotu</th>
                    <th>Operacje</th>
                </tr>
                </thead>
                {{--/*
                * zaakceptowane/wydane
                * wydane przez
                * data wydania
                * data zwrotu
                * wydana ksiazka
                *
                * */--}}
                <tbody>
                <?php /** @var \App\bookIssueLog $issueLog */ ?>
                @foreach ($bookIssueLogs as $issueLog)
                    @if(!($issueLog->trashed() || $issueLog->issue->trashed() || $issueLog->issue->book->trashed()))
                        <tr>
                            @if($allUsers)
                                <td>{{$issueLog->user->name}} {{$issueLog->user->surname}}</td>
                            @endif
                                <td>{{ $issueLog->issue->book->title }}</td>
                                <td>{{ $issueLog->approved ? 'Tak':'Nie'}}</td>
                                <td>{{ $issueLog->approved ? '-':$issueLog->issueDeadline}}</td>
                                <td>{{ $issueLog->approved ? $issueLog->librarian->name : '-' }}</td>
                                <td>{{ $issueLog->approved ? $issueLog->issuedAt : '-'  }}</td>
                                <td>{{ $issueLog->approved ? $issueLog->returnDate : '-'  }}</td>
                                <td>
                                    @if(!$issueLog->approved)
                                        {{ Form::open(['method' => 'DELETE','route' => array('bookIssueLog.destroy',$issueLog->id),'class'=>'btn-group']) }}
                                        {!! Form::submit('wycofaj rezerwacje', ['class' => 'btn btn-danger']) !!}
                                        {{ Form::close() }}
                                    @endif
                                    @can('verifyIssueLog')
                                        {{ Form::open(['method' => 'PUT', 'route' => array('bookIssueLog.update',$issueLog->id),'class'=>'btn-group']) }}
                                        @if($issueLog->approved)
                                            @if(!$issueLog->returned)
                                                {{Form::hidden('action','return')}}
                                                {!! Form::submit('Potwierdz zwrot', ['class' => 'btn btn-success']) !!}
                                            @else
                                                <div>zwrócona</div>
                                            @endif
                                        @else
                                            {{Form::hidden('action','approve')}}
                                            {!! Form::submit('Potwierdz wydanie', ['class' => 'btn btn-success']) !!}
                                        @endif
                                            {{ Form::close() }}

                                        {{--{!! Form::open(['method' => 'DELETE', 'route' => ['books.destroy', $book->id] ]) !!}
                                        {!! Form::submit('Usuń', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}--}}
                                    @endcan
                                </td>
                            {{--<td>{{ $bookIssue->avaliable ? "Tak":"Nie" }}</td>
                            <td>{{ $bookIssue->created_at }}</td>
    --}}
                        </tr>
                    @endif
                @endforeach
                </tbody>

            </table>
        {{$bookIssueLogs->links()}}


    </div>

@endsection