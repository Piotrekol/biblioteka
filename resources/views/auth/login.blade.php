@extends('layouts.app')

@section('content')
<div class="container">



                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                         <b>   E-MAIL</b>


                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                       <b> HASŁO</b>


                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                        </div>
<br/>
                        <div class="form-group">

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> <b>ZAPAMIĘTAJ MNIE</b>
                                    </label>
                                </div>

                        </div>
                        <br/>
                        <div class="form-group">

                                <button type="submit" id="btnlogin" class="btn btn-primary">
                                    ZALOGUJ SIĘ
                                </button>


                            </div>

                    </form>
    <a  class="remember" href="{{ route('password.request') }}">
    </p>Nie pamiętasz hasła?
    </a>
                </div>


@endsection
