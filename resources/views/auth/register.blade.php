@extends('layouts.app')

@section('content')
<div class="container">




                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                         <b>IMIĘ</b>


                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif

                        </div>

                        <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                           <b> NAZWISKO</b>


                                <input id="surname" type="text" class="form-control" name="surname" value="{{ old('surname') }}" required>

                                @if ($errors->has('surname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif

                        </div>




                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                        <b> ADRES ZAMIESZKANIA</b>


                                <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif

                        </div>
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <b>NUMER TELEFONU</b>


                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif

                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                       <b> E-MAIL</b>


                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <b>HASŁO
</b>




                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                        </div>






                        <div class="form-group">
                         <b>POTWIERDŹ HASŁO</b>


                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                        </div>
                        <br />
                        <div class="form-group">

                                <button id="btnlogin" type="submit" class="btn btn-primary">
                                    ZAREJESTRUJ SIĘ
                                </button>

                        </div>
                    </form>

            </div>

@endsection
